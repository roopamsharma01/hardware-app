import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HardwareItemComponent } from './hardware/hardware-list/hardware-item.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HardwareComponent } from './hardware/hardware.component';
import { HardwareListComponent } from './hardware/hardware-list/hardware-list.component';
import { HardwareDetailsComponent } from './hardware/hardware-details/hardware-details.component';

@NgModule({
  declarations: [
    AppComponent,
    HardwareComponent,
    HardwareListComponent,
    HardwareDetailsComponent,
    HardwareItemComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
