import { Component, OnInit } from '@angular/core';
import { Hardware } from './hardware'; 
@Component({
  selector: 'app-hardware',
  templateUrl: './hardware.component.html',
  styleUrls: ['./hardware.component.css']
})
export class HardwareComponent implements OnInit {
  selectedHardware: Hardware;
  constructor() { }

  ngOnInit() {
  }

}
