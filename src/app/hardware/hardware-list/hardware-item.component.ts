import { Component, OnInit, Input } from '@angular/core';
import { Hardware } from '../hardware';
@Component({
   selector: 'app-hardware-item',
  templateUrl: 'hardware-item.component.html'
})

export class HardwareItemComponent implements OnInit{
    @Input()
    hardware: Hardware;
    constructor() {}
    ngOnInit() {
    }
}
